const typeDefs = `
  type Query {
    manipulateTwoNumbers(x:Float!,y:Float!):manipulatedTwoNumbers!
    manipulateString(text:String!):manipulatedString!
    getUsersQuery:[data]!
    getUserQuery(id:Int!):[data]!
  }
  type Mutation {
    addUserMutation(name:String!,age:Int!,bday:String!,glasses:String!):isSuccessful!
    updateUserMutation(id:Int!,name:String,age:Int,glasses:getGlasses):isSuccessful!
    deleteUserMutation(id:Int!):isSuccessful!
  }
  enum getGlasses{
    Yes
    No
  }
  type manipulatedTwoNumbers{
    sum:Float!
    difference:Float!
    product:Float!
    quotient:Float!
  }
  type manipulatedString{
    lowercase:String!
    uppercase:String!
    nowhitespace:String!
  }
  type data{
    name:String!
    age:Int!
    glasses:String!
  }
  type isSuccessful{
    isSuccessful:Boolean!
  }
`

module.exports = typeDefs;