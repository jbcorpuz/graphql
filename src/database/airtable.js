const Airtable = require('airtable');
require('dotenv').config()  
const base = new Airtable ({apiKey:process.env.API_KEY}).base(process.env.BASE_KEY);
const { UserInputError } = require('apollo-server');

const getUsers = async () => {
    const usersData = [];
    const records = await base('Table 1').select({
        maxRecords: 3
    }).all()
    const data = records.forEach(function(record){
            usersData.push({ 
                name:record.get('Name'),
                age:record.get('Age'),
                glasses:record.get('Glasses'),
            }); 
        });
    return usersData
}

const getUser = async (id) => {
    const usersData = [];
    
    if (id < 0){
        throw new UserInputError('No ID with negative value', {
          argumentName: 'id'
        });
    }  else {
        const records = await base('Table 1').select({
            maxRecords: 3,
            filterByFormula:`{ID}=${id}`
        }).all()
        if(records.length === 0 ){
            throw new UserInputError('ID does not exist', {
                argumentName: 'id'
            });
        }
        const data = records.forEach(function(record){
                usersData.push({ 
                    name:record.get('Name'),
                    age:record.get('Age'),
                    glasses:record.get('Glasses'),
                }); 
            });
    }
    return usersData
}

const addUser = async (name,age,bday,glasses) => {
    let isSuccessful = false
     await base('Table 1').create([{
        "fields": {
            "Name":name,
            "Age":age,
            "Birthday": bday,
            "Glasses": glasses
        }
    }],
    isSuccessful = true
    )
    return {isSuccessful: isSuccessful};
}

const updateUser = async (id,name,age,glasses) => {
    let isSuccessful = false
    const userData = await base('Table 1').select({
        filterByFormula:`{ID}=${id}`
    }).all()
    if(userData.length > 0){
        const id = userData.map( (data) =>
              data.id
        ) 
        await base('Table 1').update(id[0],{
                "Name":name,
                "Age":age,
                "Glasses": glasses
        })
        isSuccessful = true
    }
    return {isSuccessful: isSuccessful};
}

const deleteUser = async (id) => {
    let isSuccessful = false
    const userData = await base('Table 1').select({
        filterByFormula:`{ID}=${id}`
    }).all()
    if(userData.length > 0){
        const id = userData.map( (data) =>
              data.id
        ) 
        await base('Table 1').destroy(id[0])
        isSuccessful = true
    }
    return {isSuccessful: isSuccessful};
}

module.exports = { getUsers,getUser,addUser,updateUser,deleteUser }