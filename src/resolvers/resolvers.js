
const { getUsers, getUser, addUser, updateUser, deleteUser } = require('../database/airtable')
const resolvers = {
    Query: {
      manipulateTwoNumbers:(_,{ x, y }) => {
        return{
          sum: x + y,
          difference: x - y,
          product: x * y,
          quotient: x / y
        }
      },
      manipulateString:(_,{ text }) => {
        return{
          lowercase: text.toLowerCase(),
          uppercase: text.toUpperCase(),
          nowhitespace: text.replace(/\s/g,'')
        }
      },
      getUsersQuery:() => getUsers(),
      getUserQuery: (_, {id}) => getUser(id)
    },
    Mutation : {
      addUserMutation:(_,{name,age,bday,glasses}) => addUser(name,age,bday,glasses),
      updateUserMutation:(_,{id,name,age,glasses}) => updateUser(id,name,age,glasses),
      deleteUserMutation:(_,{id}) => deleteUser(id)
    }
  }
module.exports = resolvers;